

# Adaptive Cascade

 
Adaptive Cascade is a mouse friendly and adaptive firefox theme forked from [Cascade](https://github.com/andreasgrafen/cascade) (made by [Andreas Grafen](https://andreas.grafen.info)) and [Waterfall](https://github.com/crambaud/waterfall) (made by Clément Rambaud).

**[Installation](#how-to-install-cascade) • [Screenshots](#screenshots) • [Customisation](#customise-cascade-to-your-liking)**

---
![cascade search](assets/search.png)

## How to install Adaptive Cascade


1. Type `about:config` into your URL bar. Click on the **I accept the risk** button if you're shown a warning.
2. Seach for **`toolkit.legacyUserProfileCustomizations.stylesheets`** and set it to **`true`**.
3. Go to your profile folder:
  - Linux: `$HOME/.mozilla/firefox/######.default-release/`
  - MacOS: `Users/[USERNAME]/Library/Application Support/Firefox/Profiles/######.default-release`
  - Windows: `C:\Users\[USERNAME]\AppData\Roaming\Mozilla\Firefox\Profiles\######.default-release`
4. Make and/or move into the `chrome` folder in your profile.
5. copy the `userChrome.css` file into this `chrome` folder.
6. Customise everything to your liking. (*optional*)
7. Finally install [Adaptive Tab Bar Color](https://addons.mozilla.org/en-US/firefox/addon/adaptive-tab-bar-color/).


## Screenshots


![blueberry](assets/monkeytype-blueberry.png)
![dracula](assets/monkeytype-dracula.png)
![search](assets/search.png)


## Customise Cascade to your liking

> **Note** If you're using Cascade I highly recommend to remove all clutter from the Navigation Bar area. To do so rightclick your bar and go into the customisation settings. You can move all the icons freely which allows you to place your Addons within the Personal Toolbar for example.



### Oneline Breakpoint

If you'd like to have Cascade transition into it's Oneline layout on either smaller or larger sizes you can simply do so by changing the breakpoint value in the [responsive include](chrome/includes/cascade-responsive.css) file. If you don't want Cascade to break to the Oneline layout at all remove it from the [userChrome.css](chrome/userChrome.css) file

```css
@media (min-width: 1000px) {

  […]

}
```

